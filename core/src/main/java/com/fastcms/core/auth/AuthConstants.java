package com.fastcms.core.auth;

import com.fastcms.common.constants.FastcmsConstants;

public interface AuthConstants {

    String ADMIN_RESOURCE_NAME_PREFIX = FastcmsConstants.ADMIN_MAPPING + "/";

}
