package com.fastcms.hello;

/**
 * plugin hello service
 * wjun_java@163.com
 */
public interface HelloService {

    void sayHello();

}
